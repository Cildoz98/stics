from django.apps import AppConfig


class SticsappConfig(AppConfig):
    name = 'sticsapp'
