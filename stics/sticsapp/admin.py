from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin

class SecretarioAdmin(UserAdmin):
    pass

SecretarioAdmin.fieldsets += ('Administración', {'fields': ('administrar',)}),

admin.site.register(CentroEducativo)
admin.site.register(Usuario)
admin.site.register(Profesor)
admin.site.register(Grupo)
admin.site.register(Alumno)
admin.site.register(Secretario, SecretarioAdmin)
admin.site.register(Ciudad)
admin.site.register(TipoDeDato)
admin.site.register(Mapa)
admin.site.register(Visualizar)
admin.site.register(PreguntaTextual)
admin.site.register(PreguntaRespuestaMultiple)
admin.site.register(Respuesta)
admin.site.register(Dashboard)
admin.site.register(Grafico)
admin.site.register(Curso)
admin.site.register(PertenecerA)