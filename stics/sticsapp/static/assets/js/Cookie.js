function deleteDiv(){
    var element = document.getElementById('cookies')
    element.parentNode.removeChild(element)
}

// Cookie functions from w3schools
function setCookie() {
    var d = new Date();
    d.setTime(d.getTime() + (20 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = "acceptCookies" + "=" + true + ";" + expires + ";path=/";
    deleteDiv();
}

function getCookie() {
    var name = "acceptCookies" + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(){
    if (getCookie() != ""){
        deleteDiv();
    }
}