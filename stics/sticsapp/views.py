from django.shortcuts import render
from sticsapp.models import *
from django.views.decorators.http import require_http_methods
from django.contrib.auth.hashers import make_password
from django.contrib import messages
import json
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
import requests
import io
from django.http import FileResponse
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from PIL import Image
from django.conf import settings
import psycopg2
from django.core.mail import send_mail
from textwrap import wrap
from reportlab import platypus
import csv
from random import shuffle
from django.utils.translation import gettext as _

def index(request):
	if request.user.is_authenticated:
		return Principal(request)
	else:
		return render(request, 'index.html')

def secretario(request, curso=''):
	secretario_user = Secretario.objects.get(email=request.user.email)
	centro_educativo = secretario_user.administrar

	if curso != '':
		curso = Curso.objects.get(nombre=curso, centro=centro_educativo)
	else:
		curso = Curso.objects.get(actual=True, centro=centro_educativo)
		
	curso_actual = Curso.objects.get(actual=True, centro=centro_educativo)

	if curso == curso_actual:
		actual = True
	else:
		actual = False

	if actual:
		grupos = Grupo.objects.filter(existir=secretario_user.administrar)
		alumnos = Alumno.objects.filter(estudiar_en=secretario_user.administrar)
		profesores = Profesor.objects.filter(ensenar=secretario_user.administrar)
		cursos = Curso.objects.filter(centro=centro_educativo)
	else:
		grupos = []

		alumnos_raw = PertenecerA.objects.filter(curso=curso).values('usuario').distinct()
		alumnos = []
		for alumno in alumnos_raw:
			try:
				al = Alumno.objects.get(id=alumno['usuario'])
				alumnos.append(al)
			except Alumno.DoesNotExist:
				pass

		profesores_raw = PertenecerA.objects.filter(curso=curso).values('usuario').distinct()
		profesores = []
		for profesor in profesores_raw:
			try:
				al = Profesor.objects.get(id=profesor['usuario'])
				profesores.append(al)
			except Profesor.DoesNotExist:
				pass
		
		cursos = Curso.objects.filter(centro=centro_educativo)

	return render(request, 'secretario.html', {'grupos': grupos, 'alumnos': alumnos, 'profesores': profesores, 'curso_actual': curso_actual.nombre, 'cursos': cursos, 'actual': actual, 'curso': curso})

def handler404(request):
	return render(request, '404.html')

def map_students(request, id=''):
	mapa = request.GET.get('mapa', '')
	if mapa == '':
		mapa = id
	mapa = Mapa.objects.get(id=mapa)

	if not request.session.get('num_pregunta', False):
		#Comienzo de quiz
		request.session['num_pregunta'] = 0
		request.session['num_aciertos'] = 0

	num_pregunta = request.session.get('num_pregunta', '')

	alumno = Alumno.objects.get(id=request.user.id)

	comentario = Visualizar.objects.get(mapa_referenciado=mapa, alumno_vis=alumno)

	try:
		pregunta = Pregunta.objects.filter(contener=mapa)[int(num_pregunta)]
		try:
			pregunta = PreguntaTextual.objects.get(id=pregunta.id)
			tipo = 0
		except PreguntaTextual.DoesNotExist:
			pregunta = PreguntaRespuestaMultiple.objects.get(id=pregunta.id)

			if pregunta.tipo == False:
				tipo = 2
			else:
				tipo = 1

		graficos = Grafico.objects.filter(mapa=mapa)
		return render(request, 'map_students.html', {'mapa':mapa, 'pregunta':pregunta, 'num_pregunta':num_pregunta+1, 'tipo':tipo, 'acierto':'', 'comentario':comentario.comentario, 'graficos': graficos})
	except IndexError:
		request.session['num_pregunta'] = 0
		num_aciertos = request.session['num_aciertos']
		request.session['num_aciertos'] = 0
		num_preguntas = Pregunta.objects.filter(contener=mapa).count
		graficos = Grafico.objects.filter(mapa=mapa)
		return render(request, 'map_students.html', {'mapa':mapa, 'fin':True, 'acierto':'', 'num_aciertos':num_aciertos, 'num_preguntas':num_preguntas, 'comentario':comentario.comentario, 'graficos': graficos})

def map(request, id=''):
	mapa = request.GET.get('mapa', '')
	if mapa == '':
		mapa = id
	mapa = Mapa.objects.get(id=mapa)
 
	preguntas_textuales = PreguntaTextual.objects.filter(contener=mapa)
	preguntas_respuesta_multiple = PreguntaRespuestaMultiple.objects.filter(contener=mapa)
	preguntas = list(preguntas_textuales) + list(preguntas_respuesta_multiple)

	graficos = Grafico.objects.filter(mapa=mapa)

	return render(request, 'map.html', {'mapa':mapa, 'preguntas':preguntas, 'graficos':graficos})

def olvido_contrasena(request):
	return render(request, 'olvido-contrasena.html')

def privacidad(request):
	return render(request, 'privacidad.html')

def alumno(request, curso=''):
	alumno = Alumno.objects.get(id=request.user.id)
	centro_educativo = alumno.estudiar_en

	if curso != '':
		curso = Curso.objects.get(nombre=curso, centro=centro_educativo)
	else:
		curso = Curso.objects.get(actual=True, centro=centro_educativo)
		
	curso_actual = Curso.objects.get(actual=True, centro=centro_educativo)

	if curso == curso_actual:
		actual = True
	else:
		actual = False

	cursos = Curso.objects.filter(centro=centro_educativo)

	visualizaciones = Visualizar.objects.filter(alumno_vis=alumno, curso=curso).values_list('mapa_referenciado', flat=True)
	mapas = Mapa.objects.filter(id__in=visualizaciones)

	return render(request, 'alumno.html', {'mapas':mapas, 'curso_actual': curso_actual.nombre, 'cursos': cursos, 'actual': actual, 'curso': curso})

def profesor(request, curso=''):
	profesor = Profesor.objects.get(id=request.user.id)
	centro_educativo = profesor.ensenar

	if curso != '':
		curso = Curso.objects.get(nombre=curso, centro=centro_educativo)
	else:
		curso = Curso.objects.get(actual=True, centro=centro_educativo)
		
	curso_actual = Curso.objects.get(actual=True, centro=centro_educativo)

	if curso == curso_actual:
		actual = True
	else:
		actual = False

	cursos = Curso.objects.filter(centro=centro_educativo)

	grupos = list(PertenecerA.objects.filter(curso=curso, usuario=profesor).values_list('grupo', flat=True))
	grupos = Grupo.objects.filter(id__in=grupos)

	alumnos = list(PertenecerA.objects.filter(curso=curso, grupo__in=grupos).values_list('usuario', flat=True))
	alumnos = Alumno.objects.filter(id__in=alumnos)

	mapas = Mapa.objects.filter(creado_por=profesor, curso=curso)
	localizaciones = Ciudad.objects.all()

	conn = psycopg2.connect(host="localhost",database="trafairzdb", user="trafairz", password="trafairz")
	cursor = conn.cursor()

	cursor.execute("SELECT * FROM sensor_low_cost WHERE propietario = '" + str(profesor.id) + "'")
	conn.commit()

	ns_records = cursor.fetchall()
	
	cursor.close()
	conn.close()

	return render(request, 'profesor.html', {'grupos':grupos, 'alumnos':alumnos, 'ciudades':localizaciones, 'mapas':mapas, 'nanosensores':ns_records, 'curso_actual': curso_actual.nombre, 'cursos': cursos, 'actual': actual, 'curso': curso})

def registro(request):
	centros_educativos = CentroEducativo.objects.all()
	return render(request, 'registro.html', {'centros_educativos':centros_educativos})

@require_http_methods(["POST"])
def RegistroUsuario(request):
	centro = request.POST.get('centro_educativo','')
	grupo = request.POST.get('grupo','')
	nombre = request.POST.get('nombre', '')
	apellidos = request.POST.get('apellidos', '')
	email = request.POST.get('email', '')
	contrasena = request.POST.get('contrasena', '')
	contrasena_rep = request.POST.get('contrasena_rep', '')
	categoria = request.POST.get('categoria', '')
	imagen = request.FILES.get('imagen', '')

	if contrasena != contrasena_rep:
		messages.error(request, _("Las contraseñas no coinciden"))
		centros_educativos = CentroEducativo.objects.all()
		return render(request, 'registro.html', {'centros_educativos':centros_educativos})
	
	usuario = False
	try:
		usuario = Usuario.objects.get(email=email)
	except Usuario.DoesNotExist:
		pass

	if usuario:
		messages.error(request, _("La dirección de email ya ha sido utilizada"))
		centros_educativos = CentroEducativo.objects.all()
		return render(request, 'registro.html', {'centros_educativos':centros_educativos})

	contrasena = make_password(contrasena)

	centro = CentroEducativo.objects.get(nombre=centro)
	curso = Curso.objects.get(actual=True, centro=centro)
	grupo = Grupo.objects.get(nombre=grupo, existir=centro)
	if categoria == "alumno":
		if imagen != '':
			usuario = Alumno.objects.create(first_name=nombre,last_name=apellidos,email=email,password=contrasena,username=email,foto=imagen,estudiar_en=centro)
		else:
			usuario = Alumno.objects.create(first_name=nombre,last_name=apellidos,email=email,password=contrasena,username=email,estudiar_en=centro)
		usuario.save()

		pertenencia = PertenecerA.objects.create(usuario=usuario, grupo=grupo, curso=curso)

		login(request, usuario)
		return Principal(request)
	else:
		if imagen != '':
			usuario = Profesor.objects.create(first_name=nombre,last_name=apellidos,email=email,password=contrasena,username=email,foto=imagen,ensenar=centro)
		else:
			usuario = Profesor.objects.create(first_name=nombre,last_name=apellidos,email=email,password=contrasena,username=email,ensenar=centro)
		login(request, usuario)
		return Principal(request)

@require_http_methods(["POST"])
def CargarGrupos(request):
	centro = request.POST.get('centro', '')
	centro = CentroEducativo.objects.get(nombre=centro)
	grupos = list(Grupo.objects.filter(existir = centro).values_list('nombre', flat=True))
	grupos_json = json.dumps({"grupos": grupos})
	return HttpResponse(grupos_json, content_type="application/json")

@require_http_methods(["POST"])
def Conexion(request):
	email = request.POST.get('email', '')
	contrasena = request.POST.get('contrasena', '')

	usuario = authenticate(username=email, password=contrasena)
	if usuario is not None:
		login(request, usuario)
		return Principal(request)
	else:
		messages.error(request, _("Email / contraseña inválidos"))
		return render(request, 'index.html')

def Logout(request):
	logout(request)
	return render(request, 'index.html')

def Principal(request, curso=''):
	secretario_user = Secretario.objects.filter(id=request.user.id)
	if secretario_user.count() == 0:
		alumno_user = Alumno.objects.filter(id=request.user.id)
		if alumno_user.count() == 0:
			profesor_user = Profesor.objects.filter(id=request.user.id)
			if profesor_user.count() == 0:
				logout(request)
				return index(request)
			else:
				return profesor(request, curso)
		else:
			return alumno(request, curso)
	else:
		return secretario(request, curso)

def AlumnoGrupo(request):
	alumno = request.POST.get('alumno','')
	grupo = request.POST.get('grupo','')
	curso = request.POST.get('curso','')

	alumno = Alumno.objects.get(email=alumno)
	centro = alumno.estudiar_en
	grupo = Grupo.objects.get(nombre=grupo)
	curso = Curso.objects.get(nombre=curso, centro=centro)

	if request.POST.get('anadirgrupo','') == "true":
		try:
			grupo = PertenecerA.objects.get(usuario=alumno,grupo=grupo,curso=curso)
		except PertenecerA.DoesNotExist:
			nuevo_grupo = PertenecerA.objects.get_or_create(usuario=alumno,grupo=grupo,curso=curso)
		messages.success(request, _("Alumno añadido correctamente al grupo"))
		return secretario(request)
	elif request.POST.get('eliminargrupo','') == "true":
		grupo_eliminar = PertenecerA.objects.get(usuario=alumno,grupo=grupo,curso=curso)
		grupo_eliminar.delete()
		messages.success(request, _("Alumno eliminado correctamente del grupo"))
		return secretario(request)
	else:
		alumno.delete()
		messages.success(request, _("Alumno eliminado correctamente"))
		return secretario(request)

def ProfesorGrupo(request):
	profesor = request.POST.get('profesor','')
	grupo = request.POST.get('grupo','')
	curso = request.POST.get('curso','')

	profesor = Profesor.objects.get(email=profesor)
	centro_educativo = profesor.ensenar
	grupo = Grupo.objects.get(nombre=grupo)
	curso = Curso.objects.get(nombre=curso, centro=centro_educativo)

	if request.POST.get('anadirgrupo','') == "true":
		try:
			grupo = PertenecerA.objects.get(usuario=profesor,grupo=grupo,curso=curso)
		except PertenecerA.DoesNotExist:
			nuevo_grupo = PertenecerA.objects.get_or_create(usuario=profesor,grupo=grupo,curso=curso)
		messages.success(request, _("Profesor añadido correctamente al grupo"))
		return secretario(request)
	elif request.POST.get('eliminargrupo','') == "true":
		grupo_eliminar = PertenecerA.objects.get(usuario=profesor,grupo=grupo,curso=curso)
		grupo_eliminar.delete()
		messages.success(request, _("Profesor eliminado correctamente del grupo"))
		return secretario(request)
	else:
		profesor.delete()
		messages.success(request, _("Profesor eliminado correctamente"))
		return secretario(request)
	
def EliminarGrupo(request):
	grupo = request.POST.get('grupo','')
	grupo = Grupo.objects.get(nombre=grupo)
	grupo.delete()
	messages.success(request, _("Grupo eliminado correctamente"))
	return secretario(request)

def CrearGrupo(request):
	grupo = request.POST.get('nombre','')
	centro = Secretario.objects.get(id=request.user.id).administrar
	grupo = Grupo.objects.create(nombre=grupo, existir=centro)
	messages.success(request, _("Grupo creado correctamente"))
	return secretario(request)

def CambiarFoto(request):
	imagen = request.FILES.get('imagen','')
	request.user.foto = imagen
	request.user.save()
	return Principal(request)

def CambiarDatos(request):
	nombre = request.POST.get('nombre','')
	apellidos = request.POST.get('apellidos','')
	email = request.POST.get('email','')

	if nombre != '':
		request.user.first_name = nombre
		request.user.save()
		messages.success(request, _("Nombre actualizado correctamente"))
	if apellidos != '':
		request.user.last_name = apellidos
		request.user.save()
		messages.success(request, _("Apellidos actualizados correctamente"))
	if email:
		try:
			usuario = Usuario.objects.get(email=email)
			messages.error(request, _("La dirección email ya está en uso por otro usuario"))
		except Usuario.DoesNotExist:
			request.user.email = email
			request.user.username = email
			request.user.save()
			messages.success(request, _("Email actualizado correctamente"))

	return Principal(request)

def CambiarContrasena(request):
	actual = request.POST.get('contrasena_actual','')
	nueva = request.POST.get('contrasena_nueva','')
	nueva_rep = request.POST.get('contrasena_nueva_rep','')

	usuario = authenticate(username=request.user.email, password=actual)
	if usuario is None:
		messages.error(request, _("La contraseña actual es incorrecta"))
	elif nueva != nueva_rep:
		messages.error(request, _("Las contraseñas nuevas no coinciden"))
	else:
		nueva = make_password(nueva)
		request.user.password = nueva
		request.user.save()
		login(request, request.user)
		messages.success(request, _("Contraseña actualizada correctamente"))
	
	return Principal(request)

def CrearMapa(request):
	ciudad = request.POST.get('ciudad','')
	nombre = request.POST.get('nombre','')
	oficiales = request.POST.get('oficiales','')
	nanosensores = request.POST.get('nanosensores','')
	curso = request.POST.get('curso','')

	profesor = Profesor.objects.get(id=request.user.id)
	centro_educativo = profesor.ensenar
	ciudad = Ciudad.objects.get(nombre=ciudad)
	curso = Curso.objects.get(nombre=curso, centro=centro_educativo)

	mapa = Mapa.objects.create(nombre=nombre,URL='',creado_por=profesor,localizado_en=ciudad,curso=curso)
	
	if oficiales == 'on':
		oficiales, created = TipoDeDato.objects.get_or_create(nombre="Sensores oficiales")
		oficiales.save()
		mapa.contener_informacion.add(oficiales)
		mapa.save()

	if nanosensores == 'on':
		nanosensores, created = TipoDeDato.objects.get_or_create(nombre="Nanosensores")
		nanosensores.save()
		mapa.contener_informacion.add(nanosensores)
		mapa.save()

	messages.success(request, _("El mapa ha sido creado correctamente"))

	return Principal(request)

def EliminarMapa(request):
	id = request.POST.get('eliminar','')
	Mapa.objects.get(id=id).delete()
	messages.success(request, _("Mapa eliminado correctamente"))
	return Principal(request)

def CompartirMapa(request):
	mapa = request.POST.get('compartir','')
	personas = request.POST.getlist('personas','')
	mapa = Mapa.objects.get(id=mapa)

	profesor = Profesor.objects.get(id=request.user.id)
	centro_educativo = profesor.ensenar
	curso = request.POST.get('curso','')
	curso = Curso.objects.get(nombre=curso, centro=centro_educativo)

	for persona in personas:
		try:
			#ALUMNO
			alumno = Alumno.objects.get(email=persona)

			try:
				vis_existentes = Visualizar.objects.get(mapa_referenciado=mapa, alumno_vis=alumno, curso=curso)
				messages.warning(request, _("El mapa ya había sido compartido con el alumno ") + alumno.first_name + ' ' + alumno.last_name)
			
			except Visualizar.DoesNotExist:
				visualizacion = Visualizar.objects.create(mapa_referenciado=mapa, alumno_vis=alumno, curso=curso)
				messages.success(request, _("El mapa ha sido compartido correctamente"))
				visualizacion.save()

		except Alumno.DoesNotExist:
			#GRUPO
			grupo = Grupo.objects.get(nombre=persona)

			#ALUMNOS PERTENECIENTES
			alumnos = list(PertenecerA.objects.filter(curso=curso, grupo=grupo).values_list('usuario', flat=True))
			alumnos = Alumno.objects.filter(id__in=alumnos)

			for alumno in alumnos:
				try:
					vis_existentes = Visualizar.objects.get(mapa_referenciado=mapa, alumno_vis=alumno, curso=curso)
					messages.warning(request, _("El mapa ya había sido compartido con el alumno ") + alumno.first_name + ' ' + alumno.last_name)
				
				except Visualizar.DoesNotExist:
					visualizacion = Visualizar.objects.create(mapa_referenciado=mapa, alumno_vis=alumno, curso=curso)
					messages.success(request, _("El mapa ha sido compartido correctamente"))
					visualizacion.save()

	return Principal(request)

def EditarMapa(request):
	mapa = request.POST.get('mapaEditID','')
	ciudad = request.POST.get('ciudad','')
	nombre = request.POST.get('nombre','')
	oficiales = request.POST.get('oficiales','')
	nanosensores = request.POST.get('nanosensores','')

	mapa = Mapa.objects.get(id=mapa)

	if ciudad != '':
		ciudad = Ciudad.objects.get(nombre=ciudad)
		mapa.localizado_en = ciudad
		mapa.save()

	if nombre != '':
		mapa.nombre = nombre
		mapa.save()
	
	mapa.contener_informacion.clear()
	mapa.save()

	if oficiales == 'on':
		oficiales, created = TipoDeDato.objects.get_or_create(nombre="Sensores oficiales")
		oficiales.save()
		mapa.contener_informacion.add(oficiales)
		mapa.save()

	if nanosensores == 'on':
		nanosensores, created = TipoDeDato.objects.get_or_create(nombre="Nanosensores")
		nanosensores.save()
		mapa.contener_informacion.add(nanosensores)
		mapa.save()

	messages.success(request, _('El mapa ha sido modificado correctamente'))

	return Principal(request)

def PreguntaRT(request):
	pregunta = request.POST.get('pregunta','')
	respuesta = request.POST.get('respuesta','')
	mapa = request.POST.get('mapa','')

	mapa = Mapa.objects.get(id=mapa)

	pregunta = PreguntaTextual.objects.create(titulo=pregunta, respuesta=respuesta, contener=mapa)

	return map(request)

def PreguntaRM(request):
	pregunta = request.POST.get('pregunta','')
	mapa = request.POST.get('mapa','')
	respuestas_correctas = request.POST.getlist('opcionesRM', '')

	mapa = Mapa.objects.get(id=mapa)

	if len(respuestas_correctas) > 1:
		pregunta = PreguntaRespuestaMultiple.objects.create(titulo=pregunta, contener=mapa, tipo=True)
	else:
		pregunta = PreguntaRespuestaMultiple.objects.create(titulo=pregunta, contener=mapa, tipo=False)

	i = 1
	respuestas = []

	while True:
		respuesta = request.POST.get('respuesta' + str(i),'')
		if respuesta == '':
			break
		else:
			resp = Respuesta.objects.create(texto=respuesta, es_correcta=False, pregunta=pregunta)
			respuestas.append(resp)
			pregunta.respuestas.add(resp)
			pregunta.save()

		i = i + 1

	for resp in respuestas_correctas:
		respuesta = respuestas[int(resp)-1]
		respuesta.es_correcta = True
		respuesta.save()

	return map(request)

def Correccion(request):
	request.session['num_pregunta'] = int(request.session.get('num_pregunta','')) + 1

	pregunta = request.POST.get('pregunta','')
	num_pregunta = request.POST.get('num_pregunta','')

	try:
		pregunta = PreguntaTextual.objects.get(id=pregunta)
		tipo = 0
		respuesta = request.POST.get('respuesta','')
		if pregunta.respuesta == respuesta:
			acierto = True
			request.session['num_aciertos'] = request.session['num_aciertos'] + 1
			mensaje = "¡Respuesta correcta!"
		else:
			acierto = False
			mensaje = "Respuesta incorrecta. La respuesta correcta era: " + pregunta.respuesta
	except PreguntaTextual.DoesNotExist:
		pregunta = PreguntaRespuestaMultiple.objects.get(id=pregunta)
		respuestas = pregunta.respuestas.all()
		i = 1
		num_fallos = 0
		respuestas_correctas = ''
		acierto = True

		if request.POST.get('tipo_pregunta','') == "radio":
			resp_escogida = request.POST.get('resp','nothing')
			if respuestas[int(resp_escogida)].es_correcta:
				acierto = True
			else:
				acierto = False
				for respuesta in respuestas:
					if respuesta.es_correcta:
						respuestas_correctas = respuestas_correctas + respuesta.texto
		else:
			for respuesta in respuestas:
				res = request.POST.get('respuesta' + str(i),'')
				i = i+1

				if res == 'on' and not respuesta.es_correcta:
					num_fallos = num_fallos + 1
					acierto = False
				elif res != 'on' and respuesta.es_correcta:
					num_fallos = num_fallos + 1
					acierto = False
				
				if respuesta.es_correcta:
					respuestas_correctas = respuestas_correctas + respuesta.texto + ', '

		tipo = 1

		if not acierto:
			mensaje = 'Respuesta incorrecta. Las respuestas correctas eran ' + respuestas_correctas
		else:
			mensaje = '¡Respuesta correcta!'
			request.session['num_aciertos'] = request.session['num_aciertos'] + 1

	mapa = pregunta.contener

	try:
		usuario = Alumno.objects.get(id=request.user.id)
		graficos = Grafico.objects.filter(mapa=mapa)
		return render(request, 'map_students.html', {'mapa':mapa, 'pregunta':pregunta, 'acierto':acierto, 'mensaje':mensaje, 'num_pregunta':num_pregunta, 'tipo':tipo, 'graficos':graficos})
	except Alumno.DoesNotExist:
		usuario = Profesor.objects.get(id=request.user.id)
		graficos = Grafico.objects.filter(mapa=mapa)
		return render(request, 'map.html', {'mapa':mapa, 'pregunta':pregunta, 'acierto':acierto, 'mensaje':mensaje, 'num_pregunta':num_pregunta, 'tipo':tipo, 'graficos':graficos})

def Comentar(request):
	mapa = request.POST.get('mapa','')
	comentario = request.POST.get('comentario','')

	mapa_obj = Mapa.objects.get(id=mapa)
	alumno = Alumno.objects.get(id=request.user.id)
	
	visualizacion = Visualizar.objects.get(mapa_referenciado=mapa_obj, alumno_vis=alumno)
	visualizacion.comentario = comentario
	visualizacion.save()

	return map_students(request, mapa)

def EliminarPregunta(request):
	pregunta = request.POST.get('pregunta','')
	mapa = request.POST.get('mapa','')

	Pregunta.objects.get(id=pregunta).delete()

	return map(request, mapa)

def newpage(p):
	p.showPage()
	p.rect(53,55,485,735)

def Imprimir(request):
	correccion = request.GET.get('correccion','')
	mapa = request.GET.get('id','')
	mapa = Mapa.objects.get(id=mapa)

	buffer = io.BytesIO()
	
	p = canvas.Canvas(buffer)

	p.setTitle('Cuestionario')

	w, h = A4

	p.rect(53,55,485,735)

	p.setFont("Helvetica", 18)
	if correccion == "yes":
		p.drawCentredString(w/2, 760, "Mapa de " + mapa.localizado_en.nombre + " - Cuestionario - Corrección")
	else:
		p.drawCentredString(w/2, 760, "Mapa de " + mapa.localizado_en.nombre + " - Cuestionario")

	p.drawImage(settings.STATIC_ROOT + '/assets/img/logo_transparent.png', 54, 736, 50, 50, mask='auto')

	position = 700
	counter = 1

	preguntas = Pregunta.objects.filter(contener = mapa)

	for pregunta in preguntas:
		p.setFont("Helvetica", 12)

		if counter > 9:
			wraped_text = "\n".join(wrap(preguntas[counter-1].titulo, 63))
		else:
			wraped_text = "\n".join(wrap(preguntas[counter-1].titulo, 64))
		t = p.beginText()
		t.setFont('Helvetica', 12)

		numLines = wraped_text.count('\n')
		numLines+=1
		
		try:
			if position < (numLines*60 - (numLines-1)*45 + 53):
				newpage(p)
				position = 750

			if counter > 9:
				t.setTextOrigin(85, position)
			else:
				t.setTextOrigin(80, position)
			t.textLines(wraped_text)
			p.drawString(65, position, str(counter) + ') ')
			p.drawText(t)

			pregunta = PreguntaTextual.objects.get(id=pregunta.id)
			position -= numLines*60 - (numLines-1)*45
			p.rect(80, position, 427, 50)

			if correccion == 'yes':
				wraped_text = "\n".join(wrap(pregunta.respuesta, 68))
				t = p.beginText()
				t.setFont('Helvetica', 11)

				numLines = wraped_text.count('\n')
				numLines+=1

				t.setTextOrigin(85, position+38)
				t.textLines(wraped_text)

				p.drawText(t)
			
			position -= 30

		except PreguntaTextual.DoesNotExist:
			if position < (numLines*60 - (numLines-1)*45):
				newpage(p)
				position = 750

			if counter > 9:
				t.setTextOrigin(85, position)
			else:
				t.setTextOrigin(80, position)
			t.textLines(wraped_text)
			p.drawString(65, position, str(counter) + ') ')
			p.drawText(t)

			position -= numLines*22 - (numLines-1)*8
			pregunta = PreguntaRespuestaMultiple.objects.get(id=pregunta.id)
			
			p.setFont("Helvetica", 11)
			for respuesta in pregunta.respuestas.all():
				wraped_text = "\n".join(wrap(respuesta.texto, 57))
				t = p.beginText()
				t.setFont('Helvetica', 12)

				numLines = wraped_text.count('\n')
				numLines+=1

				if position < (numLines*60 - (numLines-1)*45):
					newpage(p)
					position = 750

				if correccion == "yes" and respuesta.es_correcta:
					p.rect(80, position, 12, 12, 1, 1)
				else:
					p.rect(80, position, 12, 12)
				position+=2

				t.setTextOrigin(115, position)
				t.textLines(wraped_text)
				p.drawText(t)
				position -= numLines*22 - (numLines-1)*8

			position -= 15

		counter += 1

	p.showPage()
	p.save()

	buffer.seek(0)
	return FileResponse(buffer, as_attachment=True, filename='Cuestionario.pdf')

def CheckDashboard(request):
	id = request.GET.get('id', '')
	
	try:
		dashboard = Dashboard.objects.get(idEstacion=id)
		dashboard_json = json.dumps({"dashboard": {'idDashboard': dashboard.idDashboard, 'idEstacion': dashboard.idEstacion}})
		return HttpResponse(dashboard_json, content_type="application/json")
	except Dashboard.DoesNotExist:
		dashboard_json = json.dumps({"dashboard": None})
		return HttpResponse(dashboard_json, content_type="application/json")

def RegisterDashboard(request):
	idDashboard = request.GET.get('idDashboard', '')
	idEstacion = request.GET.get('idEstacion', '')

	dashboard = Dashboard.objects.create(idDashboard = idDashboard, idEstacion = idEstacion)

	json_response = json.dumps({"result": "correcto"})
	return HttpResponse(json_response, content_type="application/json")

def UpdateGraphs(request):
	idMapa = request.POST.get('idMapa', '')
	tipo_grafico = request.POST.get('tipo_grafico', '')
	anadir = request.POST.get('anadir', '')
	eliminar = request.POST.get('eliminar','')

	mapa = Mapa.objects.get(id=idMapa)

	if(anadir == "1"):
		try:
			grafico = Grafico.objects.get(tipo=tipo_grafico, mapa=mapa)
		except Grafico.DoesNotExist:
			grafico = Grafico.objects.create(tipo=tipo_grafico, mapa=mapa)
	else:
		try:
			grafico = Grafico.objects.get(tipo=tipo_grafico, mapa=mapa)
			grafico.delete()
		except Grafico.DoesNotExist:
			pass

	return map(request, idMapa)

def CrearSensor(request):
	latitud = request.POST.get('latitud','')
	longitud = request.POST.get('longitud','')
	modelo = request.POST.get('modelo','')
	descripcion = request.POST.get('descripcion','')
	estado = request.POST.get('estado','')
	propietario = request.user.id

	try:
		conn = psycopg2.connect(host="localhost",database="trafairzdb", user="trafairz", password="trafairz")
		cursor = conn.cursor()

		cursor.execute("INSERT INTO sensor_low_cost (MODEL, DESCRIPTION, PROPIETARIO, ESTADO) VALUES ('" + modelo + "', '" + descripcion +  "', '" + str(propietario)  +  "', '" + estado + "')")
		conn.commit()

		cursor.execute("INSERT INTO sensor_low_cost_feature (GEOM) VALUES (ST_SetSRID(ST_MakePoint(" + latitud + ", " + longitud + "), 4326))")
		conn.commit()
		
		messages.success(request, _('Sensor creado correctamente'))
	except Exception as error:
		print(error)
		messages.error(request, _('No se ha podido crear el sensor. Inténtelo de nuevo.'))

	cursor.close()
	conn.close()

	return profesor(request)

def EliminarSensor(request):
	id = request.POST.get('id', '')

	try:
		conn = psycopg2.connect(host="localhost",database="trafairzdb", user="trafairz", password="trafairz")
		cursor = conn.cursor()

		cursor.execute("DELETE FROM sensor_low_cost WHERE ID = " + str(id))
		conn.commit()

		cursor.execute("DELETE FROM sensor_low_cost_feature WHERE ID = " + str(id))
		conn.commit()
		
		messages.success(request, _('Sensor eliminado correctamente'))
	except:
		messages.error(request, _('No se ha podido eliminar el sensor. Inténtelo de nuevo.'))

	cursor.close()
	conn.close()

	return profesor(request)

def EditarSensor(request):
	latitud = request.POST.get('latitudEdit','')
	longitud = request.POST.get('longitudEdit','')
	modelo = request.POST.get('modelo','')
	descripcion = request.POST.get('descripcionEdit','')
	estado = request.POST.get('estado', '')
	id = request.POST.get('sensorEditID', '')

	try:
		conn = psycopg2.connect(host="localhost",database="trafairzdb", user="trafairz", password="trafairz")
		cursor = conn.cursor()

		if latitud != '':
			cursor.execute("UPDATE sensor_low_cost_feature SET GEOM = ST_SetSRID(ST_MakePoint(" + latitud + ", " + longitud + "), 4326) WHERE id = " + id)
			conn.commit()

		if modelo != '':
			cursor.execute("UPDATE sensor_low_cost SET model = '" + modelo + "' WHERE id = " + id)
			conn.commit()

		if estado != '':
			cursor.execute("UPDATE sensor_low_cost SET estado = '" + estado + "' WHERE id = " + id)
			conn.commit()

		if descripcion != '':
			cursor.execute("UPDATE sensor_low_cost SET description = '" + descripcion + "' WHERE id = " + id)
			conn.commit()
		
		messages.success(request, _('Sensor editado correctamente'))
	except:
		messages.error(request, _('No se ha podido editar el sensor. Inténtelo de nuevo.'))

	cursor.close()
	conn.close()

	return profesor(request)

def OlvidoContrasena(request):
	email = request.POST.get('email','')

	try:
		usuario = Secretario.objects.get(email=email)
	except Secretario.DoesNotExist:
		try:
			usuario = Alumno.objects.get(email=email)
		except Alumno.DoesNotExist:
			try:
				usuario = Profesor.objects.get(email=email)
			except Profesor.DoesNotExist:
				messages.error(request, _('No se ha encontrado el email introducido'))
				return olvido_contrasena(request)

	send_mail('Restablecimiento de contraseña', '¡Hola!\n\nAcabas de solicitar el restablecimiento de la contraseña. Si es así, pulsa en este enlace: \n\nhttp://34.90.15.171:8000/restablecer?uid=' + str(usuario.id) + '\n\nAtentamente,\n\nEl equipo de Stics', 'sticsapp@gmail.com', [usuario.email])

	return render(request, 'contrasena_mail_enviado.html')

def Restablecer(request):
	id = request.GET.get('uid','')

	return render(request, 'cambiar_contrasena.html', {'id':id})

def NuevaContrasena(request):
	nueva = request.POST.get('contrasena_nueva','')
	nueva_rep = request.POST.get('contrasena_nueva_rep','')
	id = request.POST.get('id','')

	if(nueva == nueva_rep):
		nueva = make_password(nueva)

		try:
			usuario = Secretario.objects.get(id=id)
		except Secretario.DoesNotExist:
			try:
				usuario = Alumno.objects.get(id=id)
			except Alumno.DoesNotExist:
				try:
					usuario = Profesor.objects.get(id=id)
				except Profesor.DoesNotExist:
					messages.error(request, _('No se ha podido modificar la contraseña'))
					return index(request)

		usuario.password = nueva
		usuario.save()
		messages.success(request, _('Contraseña actualizada correctamente'))
		return index(request)

	else:
		messages.error(request, _('Las contraseñas no coinciden, vuelve a intentarlo'))
		return render(request, 'cambiar_contrasena.html', {'id':id})

def AvanzarCurso(request):
	secretario_user = Secretario.objects.get(id=request.user.id)
	centro_educativo = secretario_user.administrar
	curso = Curso.objects.get(actual=True, centro=centro_educativo)
	curso.actual = False
	curso.save()

	nombre = curso.nombre
	anyos = nombre.split('/')

	anyo1 = int(anyos[0]) + 1
	anyo2 = int(anyos[1]) + 1

	curso = Curso.objects.get_or_create(nombre=str(anyo1) + "/" + str(anyo2), actual=True, centro=centro_educativo)

	return secretario(request)

def ModificarEstado(request):
	alumno = request.POST.get('alumno', '')
	estado = request.POST.get('estado', '')
	estadoSelect = request.POST.get('estadoSelect', '')

	print(estado)
	print(estadoSelect)
	
	alumno = Alumno.objects.get(email=alumno)

	if estadoSelect == "Otro":
		alumno.estado = estado
		alumno.save()
	else:
		alumno.estado = estadoSelect
		alumno.save()

	messages.success(request, _('El estado del alumno ha sido modificado correctamente'))

	return secretario(request)

def CambiarCurso(request):
	curso = request.POST.get('curso','')

	return Principal(request, curso)

@require_http_methods(["POST"])
def CargarGruposAsociados(request):
	alumno = request.POST.get('alumno', '')
	curso = request.POST.get('curso', '')
	
	if alumno == '':
		profesor = request.POST.get('profesor','')
		profesor = Profesor.objects.get(email=profesor)
		centro_educativo = profesor.ensenar
		curso = Curso.objects.get(nombre=curso, centro=centro_educativo)
		grupos = list(PertenecerA.objects.filter(curso=curso, usuario=profesor).values_list('grupo', flat=True))

		grupos = list(Grupo.objects.filter(id__in=grupos).values_list('nombre', flat=True))
	else:
		alumno = Alumno.objects.get(email=alumno)
		centro_educativo = alumno.estudiar_en
		curso = Curso.objects.get(nombre=curso, centro=centro_educativo)
		grupos = list(PertenecerA.objects.filter(curso=curso, usuario=alumno).values_list('grupo', flat=True))

		grupos = list(Grupo.objects.filter(id__in=grupos).values_list('nombre', flat=True))

	grupos_json = json.dumps({"grupos": grupos})
	return HttpResponse(grupos_json, content_type="application/json")

@require_http_methods(["POST"])
def CargarFichero(request):
	fichero = request.FILES.get('fichero','')
	grupo = request.POST.get('grupo','')
	grupo = Grupo.objects.get(nombre=grupo)

	secretario_user = Secretario.objects.get(id=request.user.id)
	centro_educativo = secretario_user.administrar
	curso = Curso.objects.get(actual=True, centro=centro_educativo)
	
	alumnos = False
	not_found = _("Los siguientes alumnos no fueron encontrados: ")
	alreadyingroup = _(". Los siguientes alumnos ya pertenecían al grupo en el curso actual: ")

	data_set = fichero.read().decode('UTF-8')
	io_string = io.StringIO(data_set)
	for column in csv.reader(io_string, delimiter=','):
		if alumnos:
			try:
				alumno = Alumno.objects.get(email=column[3])
				pertenecera = PertenecerA.objects.get(usuario=alumno, grupo=grupo, curso=curso)
				alreadyingroup += column[1] + ', ' + column[2] + "; "
			except Alumno.DoesNotExist:
				not_found += column[1] + ', ' + column[2] + "; "
			except PertenecerA.DoesNotExist:
				pertenecera_nuevo = PertenecerA.objects.create(usuario=alumno, grupo=grupo, curso=curso)
				pertenecera_nuevo.save()
		elif column[0] == "Nº":
			alumnos = True

	if not_found == _("Los siguientes alumnos no fueron encontrados: ") and alreadyingroup == _(". Los siguientes alumnos ya pertenecían al grupo en el curso actual: "):
		messages.success(request, _("Los alumnos han sido asignados al grupo " + grupo.nombre + " con éxito"))
	else:
		messages.warning(request, not_found + alreadyingroup)

	return Principal(request)

def Ventas(request):
	return render(request, 'ventas.html')

@require_http_methods(["POST"])
def FicheroCrearCuentas(request):
	fichero = request.FILES.get('fichero','')
	grupo = request.POST.get('grupo','')
	grupo = Grupo.objects.get(nombre=grupo)

	secretario_user = Secretario.objects.get(id=request.user.id)
	centro_educativo = secretario_user.administrar
	curso = Curso.objects.get(actual=True, centro=centro_educativo)
	
	alumnos = False
	already_found = _("Los siguientes alumnos ya existían: ")
	error = _(". Los siguientes alumnos no se pudieron crear: ")

	data_set = fichero.read().decode('UTF-8')
	io_string = io.StringIO(data_set)
	for column in csv.reader(io_string, delimiter=','):
		if alumnos:
			try:
				alumno = Alumno.objects.get(email=column[3])
				already_found += column[1] + ', ' + column[2] + "; "
			except:
				try:
					contrasena = column[3].split("@")
					print(contrasena[0])
					contrasena = make_password(contrasena[0])

					usuario = Alumno.objects.create(first_name=column[2],last_name=column[1],email=column[3],password=contrasena,username=column[3],estudiar_en=centro_educativo)
					usuario.save()

					pertenencia = PertenecerA.objects.create(usuario=usuario, grupo=grupo, curso=curso)
				except:
					error += column[1] + ', ' + column[2] + "; "
		elif column[0] == "Nº":
			alumnos = True

	if already_found == _("Los siguientes alumnos ya existían: ") and error == _(". Los siguientes alumnos no se pudieron crear: "):
		messages.success(request, _("Los alumnos han sido creados con éxito"))
	else:
		messages.warning(request, already_found + error)

	return Principal(request)

@require_http_methods(["POST"])
def CargaMasiva(request):
	crearcuentas = request.POST.get('crearcuentas','')
	print(crearcuentas)

	if crearcuentas == "active":
		return FicheroCrearCuentas(request)
	else:
		return CargarFichero(request)

def EditarPreguntaTextual(request):
	id_pregunta = request.POST.get('id_pregunta','')
	titulo_pregunta = request.POST.get('pregunta','')
	respuesta = request.POST.get('respuesta','')
	mapa = request.POST.get('mapa','')

	pregunta = PreguntaTextual.objects.get(id=id_pregunta)

	pregunta.titulo = titulo_pregunta
	pregunta.respuesta = respuesta
	pregunta.save()

	messages.success(request, _("Pregunta editada correctamente"))

	return map(request, mapa)

def EditarPreguntaRM(request):
	id_pregunta = request.POST.get('id_pregunta','')
	titulo_pregunta = request.POST.get('pregunta','')
	mapa = request.POST.get('mapa','')

	pregunta = PreguntaRespuestaMultiple.objects.get(id=id_pregunta)

	pregunta.titulo = titulo_pregunta
	pregunta.save()

	for respuesta in pregunta.respuestas.all():
		respuesta_escogida = request.POST.get("resp" + str(respuesta.id),'')
		respuesta = Respuesta.objects.get(id=respuesta.id)
		if respuesta_escogida == "on":
			respuesta.es_correcta = True
		else:
			respuesta.es_correcta = False
		respuesta.save()

	messages.success(request, _("Pregunta editada correctamente"))

	return map(request, mapa)

def EditarPreguntaRU(request):
	id_pregunta = request.POST.get('id_pregunta','')
	titulo_pregunta = request.POST.get('pregunta','')
	mapa = request.POST.get('mapa','')

	pregunta = PreguntaRespuestaMultiple.objects.get(id=id_pregunta)

	pregunta.titulo = titulo_pregunta
	pregunta.save()

	respuesta_escogida = request.POST.get('resp','')
	respuesta_escogida = Respuesta.objects.get(id=respuesta_escogida)
	for respuesta in pregunta.respuestas.all():
		if respuesta.id == respuesta_escogida.id:
			respuesta.es_correcta = True
		else:
			respuesta.es_correcta = False
		respuesta.save()

	messages.success(request, _("Pregunta editada correctamente"))

	return map(request, mapa)

def SettingsStudents(request):
	return render(request, 'settingstudents.html')
