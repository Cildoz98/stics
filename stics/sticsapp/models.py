from django.db import models
from django.contrib.auth.models import AbstractUser

class CentroEducativo(models.Model):
    nombre = models.CharField(max_length = 200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Centro educativo'
        verbose_name_plural = 'Centros educativos'

class Usuario(AbstractUser):
    foto = models.ImageField(upload_to='imagenes', blank=True, default='assets/img/person-icon-6.png')
    
    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

class Profesor(Usuario):
    ensenar = models.ForeignKey(CentroEducativo, on_delete=models.CASCADE)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta:
        verbose_name = 'Profesor'
        verbose_name_plural = 'Profesores'

class Grupo(models.Model):
    nombre = models.CharField(max_length = 200)
    existir = models.ForeignKey(CentroEducativo, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre + ' - ' + self.existir.nombre

class Alumno(Usuario):
    estudiar_en = models.ForeignKey(CentroEducativo, on_delete=models.CASCADE)
    estado = models.CharField(max_length=200)

    def __str__(self):
        return self.first_name + ' ' + self.last_name + ' - ' + self.estudiar_en.nombre

    class Meta:
        verbose_name = 'Alumno'
        verbose_name_plural = 'Alumnos'

class Curso(models.Model):
    nombre = models.CharField(max_length = 200)
    actual = models.BooleanField()
    centro = models.ForeignKey(CentroEducativo, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.nombre + ' - ' + self.centro.nombre

class PertenecerA(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE)

    def __str__(self):
        return self.usuario.first_name + ' ' + self.usuario.last_name + ' - ' + self.grupo.nombre + ' - ' + self.curso.nombre

    class Meta:
        verbose_name = 'Pertenecer a'
        verbose_name_plural = 'Pertenecer a'

class Secretario(Usuario):
    administrar = models.OneToOneField(CentroEducativo, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta:
        verbose_name = 'Secretario'
        verbose_name_plural = 'Secretarios'

class Ciudad(models.Model):
    nombre = models.CharField(max_length=100, primary_key=True)
    
    class Meta:
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'

class TipoDeDato(models.Model):
    nombre = models.CharField(max_length = 100, primary_key=True)

class Mapa(models.Model):
    URL = models.CharField(max_length = 200)
    nombre = models.CharField(max_length = 200)
    creado_por = models.ForeignKey(Profesor, on_delete=models.CASCADE)
    localizado_en = models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    contener_informacion = models.ManyToManyField(TipoDeDato)
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE)

class Visualizar(models.Model):
    mapa_referenciado = models.ForeignKey(Mapa, on_delete=models.CASCADE)
    alumno_vis = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    comentario = models.CharField(max_length = 1000, blank=True, null=True)
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Visualizar'
        verbose_name_plural = 'Visualizar'

class Pregunta(models.Model):
    titulo = models.CharField(max_length = 250)
    contener = models.ForeignKey(Mapa, on_delete=models.CASCADE)

class PreguntaTextual(Pregunta):
    respuesta = models.CharField(max_length = 500)

    class Meta:
        verbose_name = 'Pregunta textual'
        verbose_name_plural = 'Preguntas textuales'

class PreguntaRespuestaMultiple(Pregunta):
    tipo = models.BooleanField()

    class Meta:
        verbose_name = 'Pregunta de respuesta múltiple'
        verbose_name_plural = 'Preguntas de respuesta múltiple'

class Respuesta(models.Model):
    texto = models.CharField(max_length = 250)
    es_correcta = models.BooleanField()
    pregunta = models.ForeignKey(PreguntaRespuestaMultiple, on_delete=models.CASCADE, related_name='respuestas')

class Dashboard(models.Model):
    idDashboard = models.CharField(max_length=30, primary_key=True)
    idEstacion = models.CharField(max_length=50)

class Grafico(models.Model):
    tipo = models.IntegerField()
    mapa = models.ForeignKey(to=Mapa, on_delete=models.CASCADE)